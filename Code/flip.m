function [ out ] = flip(dataSet)
%FLIP Flip (up/down) each slice of the data Set.
%
    for i = 1:size(dataSet, 3)
        out(:, :, i) = flipud(dataSet(:, :, i));
    end
    
end

