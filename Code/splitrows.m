function [ dataSet ] = splitrows(matrix, rowSize)
%SPLITROWS Split a matrix into rowSize slices and flip each slice
%   Output is a 3D matrix

    nRows = size(matrix, 1);
    iSlice = 1;
    iLast = (nRows - rowSize + 1);
    for i = 1:rowSize:iLast;
        dataSet(:, :, iSlice)= matrix(i:(i + rowSize - 1), :);
        iSlice = iSlice + 1;
    end
    
end

