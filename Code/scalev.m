function [ out multiplier ] = scalev(X)
%SCALEV Scale the values of the matrix so they are within a reasonable
%range.
%   
    dim = length(size(X));
    
    if dim == 3
        factor = fix(log10(mean(mean(mean(abs(X)))))) - 2;
    else
        factor = fix(log10(mean(mean(abs(X))))) - 2;
    end
    
    multiplier = 10 ^ factor;
    out = X ./ multiplier;

end

