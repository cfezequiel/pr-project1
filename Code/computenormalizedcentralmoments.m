function [ out ] = computenormalizedcentralmoments(cmDataSet)

    [nSamples, nMoments] = size(cmDataSet);
    overallRms = overallrms(cmDataSet);
    for i=1:nSamples;
        for j=1:nMoments;
            out(i,j)= cmDataSet(i,j)/overallRms(:, j);
        end
    end
    
end


