function [ out ] = computeaveragecovariance(covDataSet)

    [n, m, nClasses] = size(covDataSet);
    for i=1:n
        for j=1:m    
            out(i,j) = mean(covDataSet(i, j, 1:nClasses));
        end
    end

end
