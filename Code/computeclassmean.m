function [ out ] = computeclassmean(dataSet)

    [n, m, o] = size(dataSet);
    for i = 1:o;
        out(i, :) = mean(dataSet(:, :, i));
    end

end


