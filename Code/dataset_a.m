% 
% CAP 6638 Pattern Recognition - Project 1
%
% Date: 2012-02-12
% 
% Authors:
%   Sinan Onal
%   Carlos Ezequiel
%
% Dataset A
%

clear all;
clc;

filename = 'data_setA.dat'

% Import raw data from file
rawData = importdata(filename);

% Parse raw data into a set of matrices
dataSet = splitrows(rawData, 25);

% 'Flip' each matrix of the data set
dataSet = flip(dataSet);

% Compute central moments for each sample
pqIndices = [0 0; 0 2; 0 3; 1 1; 1 2; 2 0; 2 1; 3 0];
cm = computecentralmoments(dataSet, pqIndices);

% Normalize the central moments for each sample
[ ncm ] = computenormalizedcentralmoments(cm);

% Partition the normalized central moments into a set of matrices divided 
% per class
ncmDataSet = splitrows(ncm, 10);

% Compute the class mean for each class containing 10 samples
[ classMeanValues ] = computeclassmean(ncmDataSet);

% Compute the class variance for each class containing 10 samples
[ classVarianceValues ] = computeclassvariance(ncmDataSet);

% Compute the covariance matrix for each class
[ covDataSet ] = computecovariance(ncmDataSet);

% Compute the inverse of the covariance matrix for each class, and scale it
% down.
[ invCovDataSet ] = invdataset(covDataSet);

% Scale it
[ invCovDataSet invCovMultiplier ] = scalev(invCovDataSet);

% Compute the average covariance matrix across all classes
[ aveCovMatrix ] = computeaveragecovariance(covDataSet);

% Compute the inverse average covariance matrix
[ invAveCovMatrix ] = inv(aveCovMatrix);

% Scale it
[ invAveCovMatrix invAveMultiplier ] = scalev(invAveCovMatrix);














       

