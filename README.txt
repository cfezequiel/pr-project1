%
% CAP 6638 Pattern Recognition Project  #1
%
% 2012-02-13
% 
% Sinan Onal
% Carlos Ezequiel
%

How to execute the code
-----------------------

1. In Matlab, go to the /code directory
2. Run the script 'dataset_a.m' to get the output data for dataset_A, 
   and 'dataset_b.m' for dataset_B.

   Note that both scripts use the same output variables, so running each script
   will overwrite the other.


Additional Notes
----------------
The code was tested using Matlab R2011b (7.13.0.564) 32-bit (win32).